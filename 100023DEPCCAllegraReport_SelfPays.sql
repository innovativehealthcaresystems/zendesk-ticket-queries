use IL_AGSOC_DEPCC

select
	p.number as IHSIPatientNumber
	,p.AliasName as ClientAccountNumber
	,p.MedicalRecordNo
	,p.Name as PatientName
	,cast(s.FromDate as date) as DOS
	,s.PayorNumber
	,pay.Name as PayorName
	--,pay.TypeCode as PayorTypeCode
	--,pt.Description
from patient p
join service s
	on p.Number = s.PatientNumber
join payor pay
	on s.PayorNumber = pay.Number
join IHSI_PayorType pt
	on pay.TypeCode = pt.Code
where pay.TypeCode = 'A'
	and s.entereddate >= dateadd(week,-1,getdate())
	and p.AliasName <> ''
	and s.Amount > 0.01
group by 	p.number
	,p.AliasName
	,p.MedicalRecordNo
	,p.Name
	,s.FromDate
	,s.PayorNumber
	,pay.Name
	,pay.TypeCode
	,pt.Description
order by s.PayorNumber asc, s.FromDate asc, p.AliasName asc