select
	case when (cast(rs1.DOA as date)) < '2017-07-01' then 'Before Start Date'
		else 'After Start Date'
	end as 'IHSITakeover'
	,rs1.MRN
	,cast(rs1.DOA as date) as DOA
	,rs1.HospitalVisitID
	,p1.Number as IHSIPatientNumber
	,p1.MedicalRecordNo as IHSIMRN
	,p1.AliasName as IHSIClientAccountNumber
	,csr1.PatientNumber as IHSIPatNum
	,csr1.TI_MedicalRecNo
	,csr1.VisitID
	,s.Number as ServiceNumber
	,s.ServiceItemCode
	,cast(s.FromDate as date) as DOS

	----distinct rs1.HospitalVisitID
from WCCHM_DOS_HospitalMRN rs1
left join patient p1					
	on (rs1.MRN = p1.MedicalRecordNo
		or RIGHT('000000000'+ISNULL(rs1.MRN,''),9) = p1.MedicalRecordNo
		or rs1.HospitalVisitID = p1.AliasName
		or RIGHT('000000000'+ISNULL(rs1.HospitalVisitID,''),9) = p1.AliasName
		)
left join IHSI_ClinSummaryRecord csr1
	on (rs1.MRN = csr1.TI_MedicalRecNo
		or RIGHT('000000000'+ISNULL(rs1.MRN,''),9) = csr1.TI_MedicalRecNo
		or rs1.HospitalVisitID = csr1.TI_AccountNumber
		or RIGHT('000000000'+ISNULL(rs1.HospitalVisitID,''),9) = csr1.TI_AccountNumber
		or rs1.HospitalVisitID = csr1.VisitID
		or RIGHT('000000000'+ISNULL(rs1.HospitalVisitID,''),9) = csr1.VisitID
		)
left join service s
	on (p1.number = s.PatientNumber or csr1.PatientNumber = s.PatientNumber)
where cast(rs1.DOA as date) < '2017-07-01'
	or (p1.Number is not null						---Flip this around for in versus out of our db
		or csr1.PatientNumber is not null)
group by
	rs1.MRN
	,cast(rs1.DOA as date)
	,rs1.HospitalVisitID
	,p1.Number
	,csr1.TI_MedicalRecNo
	,p1.MedicalRecordNo
	,p1.AliasName
	,csr1.PatientNumber
	,csr1.VisitID
	,s.Number
	,s.ServiceItemCode
	,s.FromDate
order by 
	p1.number asc
	,csr1.TI_MedicalRecNo asc
	,rs1.mrn asc
	,p1.MedicalRecordNo asc
	,p1.aliasname asc
	,csr1.PatientNumber asc
	,csr1.VisitID asc
	,s.Number asc
	,s.ServiceItemCode asc
	,cast(s.FromDate as date) asc
	
	----cast(rs1.DOA as date) desc






--select
--	RIGHT('000000000'+ISNULL(rs2.HMV_HOSVIS_ID,''),9) as HospitalVisitID
--	,cast(rs2.HMV_DOA as date) as DOA
--	,cast(rs2.HMV_DOD as date) as DOD
--	,cast(rs2.HMV_DOS as date) as dos 
--	--distinct rs2.HMV_HOSVIS_ID
--from HMEncounterNotAtIHSI_07022018 rs2
--group by
--	rs2.HMV_HOSVIS_ID
--	,cast(rs2.HMV_DOA as date)
--	,cast(rs2.HMV_DOD as date)
--	,cast(rs2.HMV_DOS as date)