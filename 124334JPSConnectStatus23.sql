select
	s.PatientNumber
	,p.Birthdate
	,p.Name as PtName
	,pay.Name as PayorName
	,s.FromDate
	,max(c.PrintedDate) as LastClaimPrintedDate
	,s.ServiceItemCode
	,s.Amount * s.Units as Charge
	,bbs.Balance
	,s.ServiceStatusCode
from service s
join patient p
	on s.patientnumber = p.number
join IHSI_BalanceByService bbs
	on s.Number = bbs.ServiceNumber
join payor pay
	on s.PayorNumber = pay.number
left join payment pmt
	on s.Number = pmt.ServiceNumber
left join claimservice cs
	on s.Number = cs.ServiceNumber
left join claim c
	on cs.ClaimNumber = c.Number
where s.ServiceStatusCode = 23
	and s.FromDate >= '2018-01-01'
	and s.FromDate < '2018-09-30'
	and s.PayorNumber in ('0000010','0000011')
	and pmt.number is null
group by
	s.PatientNumber
	,p.Birthdate
	,p.Name
	,pay.Name
	,s.FromDate
	,s.ServiceItemCode
	,s.Amount
	,s.Units
	,bbs.Balance
	,s.ServiceStatusCode
order by s.PatientNumber asc