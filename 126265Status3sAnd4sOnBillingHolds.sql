select
	s.number as ServiceNumber
	,s.PatientNumber
	,p.BillingStatusCode
	,p.AddressLine1
	,p.AddressLine2
	,bsc.Description as BillingStatus
	,cast(s.FromDate as date) as DOS
	,s.PayorNumber as ServicePayorNumber
	,pay.Name as ServicePayorName
	,pay.TypeCode
	,pt.Description as ServicePayorTypeDescription
	--,case
	--	when pp.Type = 'P' then'Primary'
	--	else 'Secondary'
	--end as TypeOfPayor
	,s.ServiceItemCode
	,s.ServiceStatusCode
	,s.LastUpdatedDate
	,s.Amount * s.Units as Charges
	,isnull(sum(pmt.PayAmount) + sum(pmt.AdjAmount), 0.00) as Payments
	,isnull(bs.Balance,0.00) as Balance
from service s
left join IHSI_BalanceByService bs
	on s.Number = bs.ServiceNumber
left join payor pay
	on s.PayorNumber = pay.Number
left join IHSI_PayorType pt
	on pay.TypeCode = pt.Code
left join IHSI_MGTPayments pmt
	on s.Number = pmt.svcnumber
left join patient p
	on s.PatientNumber = p.Number
left join IHSI_BillingStatusCodes bsc
	on p.BillingStatusCode = bsc.Code
--left join PatientPayor pp
--	on s.PatientNumber = pp.PatientNumber
--	and isnull(pp.DateCoverageBegins, '1900-01-01') <= s.FromDate
--	and isnull(pp.DateCoverageEnds, '2018-09-24') >= s.FromDate
--	and pp.Type in ('P','S')
where
	s.ServiceStatusCode in (3,4)
	and p.BillingStatusCode <> 'N'
group by
	s.number
	,s.PatientNumber
	,p.BillingStatusCode
	,p.AddressLine1
	,p.AddressLine2
	,bsc.Description
	,s.FromDate
	,s.PayorNumber
	,pay.Name
	,pay.TypeCode
	,pt.Description
	--,pp.Type
	,s.ServiceItemCode
	,s.ServiceStatusCode
	,s.LastUpdatedDate
	,s.Amount
	,s.Units
	,bs.Balance
order by s.ServiceStatusCode asc 
	,pay.TypeCode asc
	, s.PayorNumber asc
	, cast(s.fromdate as date) asc
