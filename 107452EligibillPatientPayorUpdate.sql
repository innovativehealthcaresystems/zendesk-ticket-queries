/* Dev Team, for each patientnumber and payornumber combo returned in the result set below, we need to update the fields PolicyNumber and GroupNumber in the PatientPayorTable. In addition if the SubscriberBirthdate does not match, we'll need to update the SubscriberName and the SubscriberBirthdate.

	Once the update is performed, change the Processed flag in the eligibil table to 1.	
*/



if
	(select
		ac.ClientType
	from IHSI_Eligibil_Import ei
	join SIVSystem.dbo.Company c
		on left(ei.TraceNumber,3) = c.CpnyID
	join CAIN.IHS_DB.dbo.IHSI_ActiveClients ac
		on c.CpnyID = ac.CpnyID
	group by
		ac.ClientType)
= 'ED'

begin	---ED code begins here
	select 
		p.Number as PatientNumber
		,pp.PayorNumber
		,ei.MemberID
		,pp.PolicyNumber	--Update to ei.MemberID
		,ei.SentPayerName
		,ei.RecdPayerName
		,ei.PtMatch
		,ei.GroupNumber as EBGroupNumber
		,pp.GroupNumber as PPGroupNumber	--Update to ei.GroupNumber
		,ei.SubscriberFirstName
		,ei.SubscriberLastName
		,ei.SubscriberMiddleName
		,ei.SubscriberSuffixName
		,pp.SubscriberName	-- Only update if condition is matched: cast(ei.SubscriberDOB as smalldatetime) <> pp.SubscriberBirthdate. Update to name parsed name: subscriberlastname + ' ' + subscribersuffixname , + subscriberfirstname + ' ' + subscribermiddlename
		,cast(ei.SubscriberDOB as smalldatetime) as BDay	
		,pp.SubscriberBirthdate	--Only update if condition is matched: cast(ei.SubscriberDOB as smalldatetime) <> pp.SubscriberBirthdate. Update to cast(ei.SubscriberDOB as smalldatetime) 
	from IHSI_Eligibil_Import ei
	join patient p
		on right(ei.TraceNumber,9) = p.Number
		and ei.Processed = 0
		--and ei.PtMatch = 'NO' --It was assumed that this row would determine necessity of updates but I think the join clause on patientpayor does a more appropriate job.
	join patientpayor pp
		on p.Number = pp.PatientNumber
		and pp.Type = 'P' --Primary payors only
		and (ei.MemberID <> pp.PolicyNumber	
			or ei.GroupNumber <> pp.GroupNumber	
			or cast(ei.SubscriberDOB as smalldatetime) <> pp.SubscriberBirthdate --If this is true, update pp.Subscriber birthdate with cast(ei.SubscriberDOB as smalldatetime). Also update pp.SubscriberName with the correct concatenation of the ei.SubscriberName fields. Otherwise, leave these fields alone to avoid re-parsing the names together
			)
		--and cast(ei.SubscriberDOB as smalldatetime) <> pp.SubscriberBirthdate --for analysis
	join IHSI_Staging st
		on p.Number = st.PatientNumber
		--and st.Verified = 'N'
end
