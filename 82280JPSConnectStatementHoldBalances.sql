select
	sum(bs.Balance) as TotalBalance
	,case
		when s.FromDate <= '2018-01-01' then 'Pre-2018'
		else '2018'
	end as ServiceGroup
from Service s
		inner join PatientPayor pp on pp.PatientNumber = s.PatientNumber and pp.Type = 'S'
		left join patient p on p.number = s.patientnumber 
		join IHSI_BalanceByService bs
			on s.Number = bs.ServiceNumber
	where pp.PayorNumber in ('0000010', '0000011', '0000216')
		and p.BillingStatusCode in ('s','y')
		and s.ServiceStatusCode not in ('40', '50')
		and case when isnull(pp.datecoverageends, '01-01-1900') = '01-01-1900' then s.fromdate else pp.datecoverageends end >= s.FromDate
		and case when isnull(pp.datecoveragebegins, '01-01-1900') = '01-01-1900' then s.fromdate else PP.datecoveragebegins end <= s.fromdate
		and s.ServiceStatusCode = '7'
	group by case
		when s.FromDate <= '2018-01-01' then 'Pre-2018'
		else '2018'
	end
