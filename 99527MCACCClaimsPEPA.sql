select
	c.number as ClaimNumber
	,cs.ServiceNumber
	,s.FromDate
	,s.ServiceItemCode
	,p.Number as PatientNumber
	,p.AliasName as HospitalAccountNo
	,isnull(dem.CSN, 'CSN Not Available') as CSN
from Claim c
join ClaimService cs
	on c.Number = cs.ClaimNumber
join Service s
	on cs.ServiceNumber = s.number
join patient p
	on s.PatientNumber = p.Number
left join IHSI_DemographicImportTemplateUpdatable dem
	on p.AliasName = dem.AliasName
where s.ServiceItemCode = 'MVACC'
and s.FromDate >= '01-01-2017'
order by s.fromdate asc