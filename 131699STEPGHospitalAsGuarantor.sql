select d.AliasName
	,d.MedicalRecNo
	,d.ServiceDate
	,d.GuarantorName as HL7GuarName
	,d.Ins1SubscriberName as INS1SubNameHL7
	,d.Ins2SubscriberName as INS2SubNameHL7
	,pp.SubscriberName
	,pp.Type
	,p.Number as PatientNumber
from IHSI_DemographicImportTemplateUpdatable d
join patient p
	on d.AliasName = p.AliasName
join PatientPayor pp
	on p.Number = pp.PatientNumber
where (d.GuarantorName like '%st%'
		and d.GuarantorName like '%tam%'
		and d.GuarantorName like '%hosp%')
	or
		(d.Ins1SubscriberName like '%st%'
		and d.Ins1SubscriberName like '%tam%'
		and d.Ins1SubscriberName like '%hosp%')
	or (d.Ins2SubscriberName like '%st%'
		and d.Ins2SubscriberName like '%tam%'
		and d.Ins2SubscriberName like '%hosp%')
group by d.AliasName
	,d.MedicalRecNo
	,d.ServiceDate
	,d.GuarantorName
	,d.Ins1SubscriberName
	,d.Ins2SubscriberName
	,pp.SubscriberName
	,pp.Type
	,p.Number

