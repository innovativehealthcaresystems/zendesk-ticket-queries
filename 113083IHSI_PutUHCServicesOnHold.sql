ALTER procedure [dbo].[IHSI_PutUHCServicesOnHold]
as 
--/*
--Created 8/6/2018 by Justin Thrash aka nA$$-T aka J-Money. Dr K. called saying stuff was crazy with their United contracts. We decided we would hold everything for x-days (should be no more than 60-90) and give the client time to figure it out. The table of the held payors was created by Renee and Marci and added to once initially. I wouldn't be surprised if it was modified again so I thought I'd make a table. Whoever is reading this, you are valued and appreciated.

--Modified 8/7/2018 by Justin per Chandy and Eric per the client. Changed it to only run on the Mandeville provider (0000003).

--Modified 9/12/2018 by Justin per Chandy et al. We no longer need to hold the patients, just need calls to be made (why? I have no clue). Modified the logic to simply store a call list daily which another query/sproc will then use to generate the weekly call list.

--Modified 11/26/2018 by Justin. Chandy caught that it was low. For some reason changes were made on 10/23 (I believe we realized we had left some patients out and were forcing a new list to generate with older accounts) and the status code it was looking for was 24. I changed it back to look for 7s and 27s.
--*/


-----------Below removed 9/12/18 per Chandy, we don't need to hold them anymore only call.
--update s
--	set s.Comment = 
--		case
--			when s.Comment = '' then 'UHC CALL PER CLIENT REQUEST'
--			else s.Comment end
--	--,s.LastUpdatedBy = '9999'
--	--,s.LastUpdatedDate = getdate()
--	,s.UserText3 = 'UHC CALL'
--	--,s.ServiceStatusCode = 99
--from service s
--join ihsi_uhcpayorholds h
--	on s.PayorNumber = h.payornumber
--	and s.ServiceStatusCode in (7,27)
--	and s.ProviderNumber = '0000003'


------------Below added 9/12/2018 to call only NOT HOLD.
insert into IHSI_UHCPatientsCalled (patientnumber, called, insertdate)

select
	distinct s.PatientNumber
	,'N'
	,getdate()
from service s
join ihsi_uhcpayorholds h
	on s.PayorNumber = h.payornumber
	and s.ServiceStatusCode in (7,27)
	and s.ProviderNumber = '0000003'
left join IHSI_UHCPatientsCalled pc
	on s.PatientNumber = pc.PatientNumber
where pc.patientnumber is null

