IF OBJECT_ID('tempdb..#CashApp') IS NOT NULL
    DROP TABLE #CashApp

DECLARE @CompanyName CHAR (30) = (SELECT CpnyName FROM SIVSystem.dbo.Company WHERE DatabaseName = DB_NAME())

CREATE TABLE #CashApp (
	PaymentNumber CHAR(9)
	,Amount FLOAT
	,EnteredDate DATE
	,PostedPeriod CHAR(6)
	,PayorType VARCHAR(40)
	,PayorTypeCode CHAR(2)
	,ProviderNumber CHAR(7)
	,ProviderAlias VARCHAR(44)
	,Company CHAR(30)
	,AdjAmount FLOAT
)

INSERT INTO #CashApp 
SELECT 
	p.Number
	,p.Amount
	,CAST(p.EnteredDate AS DATE) AS EnteredDate
	,p.PostedPeriod
	,i.Description
	,ISNULL(py.TypeCode,'')
	,s.ProviderNumber
	,pa.Alias
	,@CompanyName
	,ISNULL(SUM(a.amount),0)
FROM dbo.Payment p WITH(NOLOCK) 
	LEFT JOIN dbo.CashReceipt c WITH(NOLOCK) ON p.CashReceiptNumber = c.number
	LEFT JOIN dbo.Payor AS py WITH(NOLOCK) ON c.PayorNumber = py.Number
	LEFT JOIN dbo.IHSI_PayorType AS i WITH(NOLOCK) ON py.typecode = i.code
	LEFT JOIN dbo.Service AS s WITH(NOLOCK) ON s.Number = p.ServiceNumber
	LEFT JOIN dbo.IHSI_providerAlias AS pa WITH (NOLOCK) ON s.ProviderNumber = pa.ProviderNumber
	LEFT JOIN dbo.Adjustment AS a WITH (NOLOCK) ON p.Number = a.PaymentNumber AND a.TransType = 'P'
GROUP BY 
	p.Number
	,p.Amount
	,CAST(p.EnteredDate AS DATE)
	,p.PostedPeriod
	,i.Description
	,py.TypeCode
	,s.ProviderNumber
	,pa.Alias

SELECT
	ca.Company
	,ca.ProviderAlias
	,ca.ProviderNumber
	,ca.PayorType
	,ca.PayorTypeCode
	,ca.EnteredDate
	,ca.PostedPeriod
	,SUM(ca.Amount) + SUM(ca.AdjAmount) as PaymentAmount
FROM #CashApp ca
GROUP BY
	ca.Company
	,ca.ProviderAlias
	,ca.ProviderNumber
	,ca.PayorType
	,ca.PayorTypeCode
	,ca.EnteredDate
	,ca.PostedPeriod
	
DROP TABLE #CashApp