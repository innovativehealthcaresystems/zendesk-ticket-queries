--insert into PatientNote
--(
--EnteredBy
--,EnteredDate
--,NoteField1
--,NoteField2
--,Number
--,PatientNumber
--,UserDate1
--,UserDate2
--,UserText1
--,UserText2
--,Type
--)

select 
	'9999' as EnteredBY
	,GETDATE() as EnteredDATE
	,'TTC ERROR' as NoteField1
	,'Turned in Error letter mailed out week of 18-0813' as NoteField2
	,Replicate('0',+4)+cast((select max(number)  from PatientNote) + ROW_NUMBER() over (Order by (select null))as varchar) as PatientNotenumber
	--,p.Name as PatientName
	--,isnull(pp.SubscriberName,pp2.SubscriberName) as GuarantorName
	--,pp2.SubscriberName
	--,tla.Bill_to_Address as AddressLine1
	--,tla.Bill_To_Address2 as AddressLine2
	--,tla.City
	--,tla.State
	--,tla.Zip
	,p.Number as PAccountNumber
	,'1900-01-01 00:00:00' as UserDate1
	,'1900-01-01 00:00:00' as UserDate2
	,'' as UserText1
	,'' as UserText2
	,'S' as Type
	--,Timestamp as tstamp

from IHSI_ReversedTurns rt
join IHSI_TransfinancialLetterAddresses tla
	on rt.PatientNumber = tla.Account_No
	and tla.Bill_to_Address not like '%bad%'
left join PatientPayor pp
	on rt.PatientNumber = pp.PatientNumber
	and pp.Type = 'G'
join PatientPayor pp2
	on rt.PatientNumber = pp2.PatientNumber
	and pp2.Type = 'P'
join Patient p
	on rt.PatientNumber = p.Number
	and p.number not like '%038728'	--Filter out patients form Sharon's file sne 8/9/18 of patients who have paid in full to the collection agency
	and p.number not like '%034599'
	and p.number not like '%037628'
	and p.number not like '%034789'
	and p.number not like '%033713'
	and p.number not like '%038197'
	and p.number not like '%038249'
	and p.number not like '%034983'
	and p.number not like '%040145'
	and p.number not like '%040857'
	and p.number not like '%037715'
	and p.number not like '%038008'
	and p.number not like '%037114'
	and p.number not like '%039341'
	and p.number not like '%034271'
	and p.number not like '%035225'
	and p.number not like '%040881'
	and p.number not like '%038862'
	and p.number not like '%037522'
	and p.number not like '%034102'
	and p.number not like '%034458'
	and p.number not like '%033880'
	and p.number not like '%037532'
	and p.number not like '%038657'
	and p.number not like '%040136'
	and p.number not like '%035695'
	and p.number not like '%034386'
	and p.number not like '%0112802'
group by p.Name	
	,pp.SubscriberName
	,pp2.SubscriberName
	,tla.Bill_to_Address
	,tla.Bill_To_Address2
	,tla.City
	,tla.State
	,tla.Zip
	,p.Number
order by p.Number asc