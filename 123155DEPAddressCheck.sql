select 
	p.Number
	,p.AliasName
	,dem.PatientAddress1
	,dem.PatientAddress2
	,dem.PatientCity
	,dem.PatientState
	,dem.PatientZip
	,max(dem.HL7MessageTime)
from patient p
left join IHSI_DemographicImportTemplateUpdatable dem
	on p.AliasName = dem.AliasName
where p.number in
	('000018439'
	,'000023694'
	,'000023850'
	,'000023850'
	,'000029756'
	,'000029756'
	,'000028764'
	,'000024388'
	,'000023821'
	,'000023821'
	,'000023821'
	,'000012797'
	,'000015173'
	,'000023623'
	,'000023623'
	,'000029222'
	,'000020841'
	,'000024696'
	,'000005213'
	,'000005213'
	,'000005213'
	,'000009239'
	,'000021099'
	,'000015702'
	,'000015702'
	,'000015702'
	,'000023963'
	,'000023963'
	)
	and dem.PatientAddress1 not like '%address%'
	and dem.PatientAddress1 not like '%unk%'
	and dem.PatientAddress1 not like '%homeless%'
	and dem.PatientAddress1 not like '%see%'
group by
	p.Number
	,p.AliasName
	,dem.PatientAddress1
	,dem.PatientAddress2
	,dem.PatientCity
	,dem.PatientState
	,dem.PatientZip