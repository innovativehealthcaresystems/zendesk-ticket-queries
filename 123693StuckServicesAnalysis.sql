select
	x.ServiceNumber
	,x.PatientNumber
	,x.BillingStatusCode
	,x.Description
	,x.Balance
	,x.DOS
	,x.LastStatementDate
	,x.StatusChangeDate
	,x.DaysSinceLastStatement
	,x.LastStatementRun
	,x.LastTurnDate
from
(	
	select
	s.Number as ServiceNumber
	,s.PatientNumber
	,p.BillingStatusCode
	,bsc.Description
	,bs.Balance
	,s.FromDate as DOS
	,max(ps.PrintedDate) as LastStatementDate
	,max(aud.audit_changed) as StatusChangeDate
	,datediff(day,max(ps.printeddate),getdate()) as DaysSinceLastStatement
	,(select
		max(printeddate)
	from PatientStatement) as LastStatementRun
	,(select
		max(entereddate)
	from adjustment
	where AdjustmentTypeCode in ('045','080')) as LastTurnDate
from service s
left join IHSI_BalanceByService bs
	on s.Number = bs.ServiceNumber
left join PatientStatement ps
	on s.PatientNumber = ps.PatientNumber
left join Patient p
	on s.PatientNumber = p.Number
left join IHSI_BillingStatusCodes bsc
	on p.BillingStatusCode = bsc.Code
left join AuditService aud
	on s.Number = aud.Number
	and aud.ServiceStatusCode <> 27
where s.ServiceStatusCode in ('27')
group by
	s.Number
	,s.PatientNumber
	,p.BillingStatusCode
	,bsc.Description
	,bs.Balance
	,s.FromDate
)x
where x.DaysSinceLastStatement >= 180
	and x.StatusChangeDate <= x.LastStatementRun
	--and x.StatusChangeDate <= x.LastTurnDate
order by x.StatusChangeDate desc
