begin transaction

delete st

--select 
--	mcl.Tl_AccountNumber
--	,st.ti_dateofservice
--	,st.Staging_ID

from PEPA_Missing_Chart_List mcl
join patient p
	on mcl.Tl_AccountNumber = p.AliasName
left join service s
	on p.Number = s.PatientNumber
join IHSI_Staging st
	on p.Number = st.PatientNumber
left join IHSI_ImageFiles i
	on st.Staging_ID = i.Staging_ID
left join IHSI_SkipLog sl
	on st.Staging_ID = sl.Staging_ID
where i.ImageFileName is null
	and s.ServiceItemCode is null
--group by
--	mcl.Tl_AccountNumber
--	,st.ti_dateofservice
--	,st.Staging_ID

if @@ROWCOUNT < 17000
commit transaction

else rollback
go
--order by TI_DateOfService desc