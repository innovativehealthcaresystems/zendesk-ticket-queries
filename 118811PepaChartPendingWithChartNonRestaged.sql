select
	s.PatientNumber
	,p.AliasName
	,st.Staging_ID
	,st.Verified
	,st.Coded
from service s
join patient p
	on s.PatientNumber = p.Number
	and s.PayorNumber = '4444444'
join IHSI_ImageFiles i
	on p.AliasName = i.TI_AccountNumber
	and i.ImageFileName not like '%dem%'
	and i.ImageFileName not like '%elgb%'
join IHSI_Staging st
	on s.PatientNumber = st.PatientNumber
	and st.Verified = 'Y'
group by 
	s.PatientNumber
	,p.AliasName
	,st.Staging_ID
	,st.Verified
	,st.Coded

