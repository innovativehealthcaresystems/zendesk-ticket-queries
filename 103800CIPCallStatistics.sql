select
	cl.ID
	,cl.UserName
	,cast(cl.StartTime as Date) as CallDate
	,cl.StartTime
	,cl.EndTime
	,datediff(minute, cl.StartTime, cl.EndTime) as CallMinutes
	,cl.UserID
	,vl.PatientNumber
	,vl.DatabaseNumber
	,ac.CpnyName
	,cd.DispositionID
	,d.Description
	,cd.AdditionalNotes
	--,vl.VerifyStartTime
	--,vl.VerifyEndTime
	--,vl.UserName
	--,vl.ID
	--,vl.UserID
	--,vl.CallID	
	--,*
	,datepart(year, cl.StartTime) as CallYear
	,datepart(month, cl.starttime) as CallMonth
from PSR_CallLog cl
left join PSR_PatientVerificationLog vl
	on cl.ID = vl.CallID
left join PSR_CallDisposition cd
	on vl.ID = cd.VerifyID
left join PSR_Dispositions d
	on cd.DispositionID = d.RespCode
left join IHS_DB.dbo.IHSI_ActiveClients ac
	on vl.DatabaseNumber = ac.CpnyID
where 
--vl.DatabaseNumber = '367'
--	and 
	cl.StartTime >= '2018-07-23'
	--and cl.endtime is not null
order by vl.DatabaseNumber asc, cl.starttime asc