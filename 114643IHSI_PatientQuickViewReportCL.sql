ALTER procedure [dbo].[IHSI_PatientQuickViewReport]
@showzeroBalance int
as
begin
		declare @sql varchar(max),@wherenotshowzeroBalance1 varchar(100),@whereshowzeroBalance2 varchar(100)
		
		if(@showzeroBalance=1)
			begin 
				set @wherenotshowzeroBalance1=''
				
			end
		else
			begin
				set	@wherenotshowzeroBalance1='where round(q.Balance,1)>0
			or round(q.Balance,1)<= -1'
			end
		
		create table #tempdataCL
		(
			Summary_ID int,
			MedicalRecordNo varchar(100),
			DateOfService date,
			Number char(9),
			Name varchar(100),
			PhoneNo1 varchar(100),
			PhoneNo2 varchar(100),
			AddressLine1 varchar(100),
			Birthdate date,
			City varchar(100),
			State varchar(100),
			ZipCode varchar(100),
			SocialSecurityNo varchar(100),
			PatientBalance float,
			InsuranceBalance float,
			OtherBalance float,
			TransDate date,
			Balance float
		)
			
		set @sql='select 
			c.Summary_ID,
			MedicalRecordNo,
			convert(date,TI_DateOfService) as DateOfService,
			p.Number,
			Name,
			p.PhoneNo1,
			p.PhoneNo2,
			p.AddressLine1,
			convert(date,Birthdate) as Birthdate,
			City,
			State,
			p.ZipCode,
			p.SocialSecurityNo,
			patientbalance=
			Case when Q.servicestatuscode in(7,27) then Balance end,
			InsuranceBalance=Case when Q.servicestatuscode in(3,4,23,24) then Balance end,
			otherbalance=case when Q.servicestatuscode not in(7,27,3,4,23,24) then Balance end,
			convert(date,TransDate) as TransDate,
			Balance
		from  IHSI_ClinSummaryRecord c
			inner join IHSI_ClinDetailRecord d
				on d.Summary_ID=c.Summary_ID
			inner join IHSI_QuickViewActivity q
				on c.PatientNumber=q.patientnumber
				and d.TI_DateOfService=q.TransDate
			inner join Patient p
				on p.Number=c.PatientNumber			
			inner join IHSI_CreditStatusCodes s
				on s.Code=p.CreditStatusCode
			inner join IHSI_BillingStatusCodes b 
				on b.Code=p.BillingStatusCode
			'+@wherenotshowzeroBalance1+''
			
		insert into #tempdataCL
		exec (@sql)	
		
		SELECT
			Summary_ID,
			MedicalRecordNo,
			DateOfService,
			Number,
			Name,
			PhoneNo1,
			PhoneNo2,
			AddressLine1,
			Birthdate,
			City,
			State,
			ZipCode,
			SocialSecurityNo,
			patientbalance,
			InsuranceBalance,
			otherbalance,
			SUM(Balance) as TotalBalance
		FROM #tempdataCL
		GROUP BY
			Summary_ID,
			MedicalRecordNo,
			DateOfService,
			Number,
			Name,
			PhoneNo1,
			PhoneNo2,
			AddressLine1,
			Birthdate,
			City,
			State,
			ZipCode,
			SocialSecurityNo,
			patientbalance,
			InsuranceBalance,
			otherbalance
		order by DateOfService
			
			drop table #tempdataCL
end	