--USE [TX_BSW_CIP]
--GO
--/****** Object:  StoredProcedure [dbo].[IHSI_VerificationQueueByHoursQueue]    Script Date: 9/27/2018 9:04:03 AM ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--ALTER PROCEDURE [dbo].[IHSI_VerificationQueueByHoursQueue]
--AS

/*
-- =============================================
-- Author:		Sriram.C
-- Create date: 09-MAY-2017
-- Description:	Created for the modified ICIS ED Verification Screen in version 1.1.1.100 per SR 40179. It takes the user to the most recent chart based on BCD date
--				when the user clicks 'Verify Available Charts'. It also populates the verification screen when the user checks 'Show Queue'.
-- Note:		
-- Entity Type: ED Verification Queue
--
-- Last Modified:

	- 7/17/2017 by Margaret W. Added a get out of purgatory filter for records without all images that are a certain # of days old for PEMM DBs.
	
	- 7/15/2017 by Justin T. and Margaret - Added logic to ensure that records go into the queue that have both a facesheet and a chart for PEMM DBs.

	- 6/8/2017 by Sriram C.
	
	- 5/31/2017 by Justin T. 

	
-- =============================================
IHSI_VerificationQueueByHoursQueue
*/
	
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--select
--	st.staging_id as staging_id,
--	Case
--		WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
--		WHEN MAX(i.StagedDate)<=st.stagedDate THEN MAX(st.StagedDate) --Modified by Justin T. on 5/31/2017 - changed output from MAX(I.STAGEDDATE) to MAX(ST.STAGEDDATE)
--		END AS MostRecentDate

--from IHSI_Staging as st
--	left join IHSI_ImageFiles as i on st.staging_id=i.staging_id
--WHERE ST.SKIPPED='N'
--	AND ST.Verified = 'N'
--	AND ST.VerifyLock = 'N'		-- Added by Justin T. on 5/31/2017 as found in IHSI_VerificationQueue
--Group by st.StagedDate,st.staging_id
--having COUNT(i.image_id)>=1
--order by MostRecentDate asc	

DECLARE @today DATETIME =  DATEADD(DAY, DATEDIFF(DAY,0, GETDATE()), 0)
DECLARE @PurgatoryDays INT = CASE WHEN DB_NAME() IN 
('')
--('LA_WCCH_CCHMG') --Removed by Justin per Jeff
THEN -11 ELSE -6 END 	
--12 days for CCEPG/CCHMG, 7 days for all other PEMM DBs. The logic takes the current day @midnight. 
--LA_WCCH_CCEPG not included because of temporary roadblocks for that DB
--The filtering logic down below takes records that are less than the current day at midnight -6 (or whatever the variable is) days to get all records
--that are 7 days old, regardless of time (records that are older than 6 days ago at midnight)
--added by Margaret on 7/17/2017 to account for variable days in purgatory 
--this will need to be revised later for other DBs that need purgatory logic 
		
IF DB_NAME ()IN 
('')
--('LA_LCMH_PEMMLC','LA_STPH_STEPG','LA_WCCH_CCHMG')   --Removed by Justin per Jeff
--added by Margaret on 7/16/2017 to account for PEMM DBs
--LA_OLOA_WPEPG not included because of temporary manual verificaiton for that DB
--LA_WCCH_CCEPG not included because of temporary roadblocks for that DB
--LA_TCRM not included for same reason
BEGIN 		
	select
		st.staging_id as staging_id,
		Case
			WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
			WHEN MAX(ISNULL (I.STAGEDDATE, '1900-01-01'))<=ST.STAGEDDATE THEN MAX(ST.STAGEDDATE)	
			--Modified by Justin T. on 5/31/2017 - changed output from MAX(I.STAGEDDATE) to MAX(ST.STAGEDDATE)
			--Modified by Margaret on 7/19/2017 - added is null to account for records moved back from purgatory 
		END AS MostRecentDate,
		st.ti_dateofservice, i.ti_accountnumber, ISNULL(p1.typecode, 'XX') AS PayorType, 
					'NA' as SkipReason, 'NA' as StatusCheck, st.PatientNumber
	from IHSI_Staging as st
		left join IHSI_ImageFiles as i on st.staging_id=i.staging_id
			AND CHARINDEX('_DEM_', i.ImageFileName) <> 0	--Added by Justin T. on 7/15/2017 to make sure that the record has a facesheet
			AND EXISTS(SELECT i2.ImageFileName FROM IHSI_ImageFiles i2 WHERE st.Staging_ID = i2.Staging_ID		--Added by Justin T. on 7/15/2017 to make sure that the record has a chart
				AND CHARINDEX('_DEM_', i2.ImageFileName) = 0)
		LEFT JOIN dbo.PatientPayor as PP1 on pp1.patientnumber = st.patientnumber AND pp1.type = 'P' 
		LEFT JOIN dbo.Payor as P1 on P1.number = pp1.payornumber	
	WHERE ST.SKIPPED='N'
		AND ST.Verified = 'N'
		AND ST.VerifyLock = 'N'		-- Added by Justin T. on 5/31/2017 as found in IHSI_VerificationQueue
	Group by st.StagedDate,st.staging_id,st.ti_dateofservice, i.ti_accountnumber, ISNULL(p1.typecode, 'XX'),st.PatientNumber
	having (COUNT(i.image_id)>=1) OR (st.StagedDate < DATEADD(DD, @PurgatoryDays, @today)) --added by Margaret to get records out of purgatory
	order by MostRecentDate asc		
END	
ELSE 
BEGIN
select
	x.staging_id
	,x.TI_DateOfService
	--,i.ImageFileName
	,case
		when dem.AliasName is null then 'Not Received'
		else 'Received'
	end as Demographic
	,dem.AliasName
	,x.TI_AccountNumber
from (
select
		st.staging_id as staging_id,
		Case
			WHEN MAX(i.StagedDate)>st.stagedDate THEN MAX(i.StagedDate)
			WHEN MAX(i.StagedDate)<=st.stagedDate THEN MAX(st.StagedDate) --Modified by Justin T. on 5/31/2017 - changed output from MAX(I.STAGEDDATE) to MAX(ST.STAGEDDATE)
		END AS MostRecentDate,
		st.ti_dateofservice, i.ti_accountnumber, ISNULL(p1.typecode, 'XX') AS PayorType, 
					'NA' as SkipReason, 'NA' as StatusCheck, st.PatientNumber
	from IHSI_Staging as st
		left join IHSI_ImageFiles as i on st.staging_id=i.staging_id
		LEFT JOIN dbo.PatientPayor as PP1 on pp1.patientnumber = st.patientnumber AND pp1.type = 'P' 
		LEFT JOIN dbo.Payor as P1 on P1.number = pp1.payornumber	
	WHERE ST.SKIPPED='N'
		AND ST.Verified = 'N'
		AND ST.VerifyLock = 'N'		-- Added by Justin T. on 5/31/2017 as found in IHSI_VerificationQueue
	Group by st.StagedDate,st.staging_id,st.ti_dateofservice, i.ti_accountnumber, ISNULL(p1.typecode, 'XX'),st.PatientNumber
	having COUNT(i.image_id)>=1
	--order by MostRecentDate asc
	)x
left join IHSI_ImageFiles i
	on x.staging_id = i.Staging_ID
	and i.ImageFileName not like '%dem%'
left join IHSI_ImageFiles i2
	on x.staging_id = i2.Staging_ID
	and i2.ImageFileName like '%dem%'
left join IHSI_ImageFiles i3
	on x.staging_id = i3.Staging_ID
	and (i3.ImageFileName like '%forney%'
		or i3.ImageFileName like '%lakepointe%'
		or i3.ImageFileName like '%wylie%')
left join IHSI_DemographicImportTemplateUpdatable dem
	on x.TI_AccountNumber = dem.AliasName
where i2.Image_ID is null
	--and dem.AliasName is null
	and i3.Image_ID is null
group by
	x.staging_id
	,x.TI_DateOfService
	--,i.ImageFileName
	,dem.AliasName
	,x.TI_AccountNumber
order by x.staging_id desc

END	
	
END


