UPDATE dbo.IHSI_Staging
SET Verified = 'N'
    , VerifiedByID = NULL
    , VerifiedDate = NULL
    , VerifyLock = 'N'
    , VerifyStartTime = NULL
    , VerifyEndTime = NULL
WHERE Staging_ID in
(
	select
		st.Staging_ID
		--,st.verified
		--,st.verifieddate
		--,i1.ImageFileName as DemImage
		--,i1.StagedDate as DemStageDate
		--,i2.ImageFileName as DemFileName
		--,i2.StagedDate as ChartStageDate
	from ihsi_staging st
	join IHSI_ImageFiles i1
	on st.TI_AccountNumber = i1.TI_AccountNumber
		and i1.ImageFileName like '%dem%'
	join ihsi_imagefiles i2
		on st.TI_AccountNumber = i2.TI_AccountNumber
		and i2.ImageFileName not like '%dem%'
		and i2.ImageFileName not like '%nsbep%'
	where i2.StagedDate > st.VerifiedDate
		and i1.StagedDate < st.VerifiedDate
		and not exists (select staging_id 
						from IHSI_ImageFiles 
						where StagedDate < st.VerifiedDate
							and ImageFileName not like '%dem%'
							and TI_AccountNumber = st.TI_AccountNumber
						)
		and i2.image_id > '2676000'
)
