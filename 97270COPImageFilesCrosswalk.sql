use TX_BSW_COP

select
	DB_NAME() as DatabaseName
	,i.ImageFileName
	,i.StagedDate
	,i.TI_AccountNumber as ClientAccountNumber
	,p.MedicalRecordNo
	,p.Number as IHSIPatientNumber
from IHSI_ClinImageFiles i
join patient p
	on i.TI_AccountNumber = p.AliasName
where i.TI_AccountNumber <> ''
order by p.Number asc