select
	AliasName
	,Ins1Name
	,Ins1PolicyNumber
	,Ins2Name
	,Ins2PolicyNumber
	,ServiceDate
	,HL7MessageTime
	--,i.FileLocation + i.ImageFileName
	--,i.ImageFileName
from IHSI_DemographicImportTemplateUpdatable dem
join IHSI_ImageFiles i
	on dem.AliasName = i.TI_AccountNumber
	and i.ImageFileName like '%dem%'
where Ins1Name = 'MEDICARE'
	--and AliasName = '137692836'
order by aliasname desc