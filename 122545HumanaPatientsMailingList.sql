--select		
--	p.Name as PatientName
--	,p.AddressLine1
--	,p.AddressLine2
--	,p.City
--	,p.State
--	,p.ZipCode
--	,pay.Name as PayorName
--from PatientPayor pp
--join payor pay
--	on pp.PayorNumber = pay.Number
--	and pay.TypeCode = 'M'
--join patient p
--	on pp.PatientNumber = p.Number
--join service s
--	on p.Number = s.PatientNumber
--	and s.FromDate >= '2016-01-01'
--	and s.PayorNumber = pp.PayorNumber
--	and pay.TypeCode = 'M'
--	and s.PatientNumber = pp.PatientNumber
--where 
--	(isnull(pp.DateCoverageEnds, getdate()) >= getdate()
--		or pp.DateCoverageEnds = '1900-01-01')
--group by
	--p.Name
	--,p.AddressLine1
	--,p.AddressLine2
	--,p.City
	--,p.State
	--,p.ZipCode
	--,pay.Name

select
	p.Name
	,
	p.AddressLine1
	,p.AddressLine2
	,p.City
	,p.State
	,p.ZipCode
	,pay.Name
from service s
join PatientPayor pp
	on s.FromDate >= '2016-01-01'
	and s.PatientNumber = pp.PatientNumber
	and s.PayorNumber = pp.PayorNumber
	and (isnull(pp.DateCoverageEnds,getdate()) >= getdate()
		or pp.DateCoverageEnds = '1900-01-01')
join payor pay
	on pp.PayorNumber = pay.Number
	and pay.TypeCode = 'M'
join patient p
	on s.PatientNumber = p.Number
group by 	
	p.Name
	,
	p.AddressLine1
	,p.AddressLine2
	,p.City
	,p.State
	,p.ZipCode
	,pay.Name
