select s.PatientNumber
	,pat.MedicalRecordNo
	,cast(s.FromDate as date) as DOS
	,min(s.ServiceItemCode) as ServiceItemCode
	,s.ICDTen1
	,it.ParentCode
	,it.Description
	,sum(p.PayAmount + p.AdjAmount) as Payments
from service s
left join IHSI_MGTPayments p	
	on s.Number = p.svcnumber
	and p.PayAmount + p.AdjAmount <> 0
join patient pat
	on s.PatientNumber = pat.number
join ICDTen it
	on s.ICDTen1 = it.Code
where s.fromdate >= '2017-01-01'
	and s.FromDate < '2018-01-01'
	and s.ServiceItemCode in ('99281'
		,'99282'
		,'99283'
		,'99284'
		,'99285'
		,'99291'
		,'99201'
		,'99202'
		,'99211'
		,'99212'
		,'99213'
		,'99214'
		,'99215'
		)
	and s.TransToSvcNum = ''
	and s.Amount * s.units > 0.01
group by s.PatientNumber
	,s.ICDTen1
	,cast(s.FromDate as date)
	,pat.MedicalRecordNo
	,it.ParentCode
	,it.Description
order by s.ICDTen1 asc
