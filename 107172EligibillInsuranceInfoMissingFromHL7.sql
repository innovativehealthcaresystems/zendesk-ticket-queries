use LA_OLOL_PEPA

select
	right(e.Trace_Number,9) as PatientNumber
	,p.AliasName as HospitalAccountNumber
	,e.Member_ID
	,e.Sent_Payer_Name
	,e.Rec_d_Payer_Name
	,cast(e.service_date as date) as DOS
	--,d.HL7MessageTime\
	,d.Ins1Name
	,d.Ins1PolicyNumber
	,max(d.HL7MessageTime) as MostRecentHL7MessageTime
	--i.FileLocation
	,max(i.ImageFileName) as ImageFileName
	--,i.FileLocation + max(i.imagefilename) as PathAndFileName
	,DATEDIFF(day,cast(e.service_date as date),cast(max(d.hl7messagetime) as date)) as MessageLatencyDays
	,case
		when e.Member_ID = d.Ins1PolicyNumber then 'Y'
		else 'N'
		end as PolicyNumberMatch
	--,i.StagedDate
from ihsi_eligibility_712 e
join patient p
	on right(e.Trace_Number,9) = p.Number
join IHSI_DemographicImportTemplateUpdatable d
	on p.AliasName = d.AliasName
	and cast(d.HL7MessageTime as date) > '2018-01-01'
	and (e.Member_ID <> isnull(d.Ins1PolicyNumber,'')) 
join IHSI_ImageFiles i
	on p.AliasName = i.TI_AccountNumber
	and i.ImageFileName like '%dem%'
left join IHSI_DemographicImportTemplateUpdatable d1
	on d.AliasName = d1.AliasName
	and e.Member_ID = d1.Ins1PolicyNumber
where cast(e.service_date as date) >= '2018-03-01'
	and cast(e.service_date as date) < '2018-06-01'
	and Pt_Match = 'NO'
	and d1.AliasName is null
group by
	right(e.Trace_Number,9)
	,p.AliasName
	,e.Member_ID
	,e.Sent_Payer_Name
	,e.Rec_d_Payer_Name
	,cast(e.service_date as date)
	--,d.HL7MessageTime
	,d.Ins1Name
	,d.Ins1PolicyNumber
	--,d.HL7MessageTime
	,i.FileLocation
	--,i.ImageFileName
	--,i.StagedDate
order by
 --i.stageddate desc
	p.AliasName asc
	,d.Ins1PolicyNumber desc 
	----,d.HL7MessageTime desc