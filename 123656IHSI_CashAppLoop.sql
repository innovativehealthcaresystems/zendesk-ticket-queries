ALTER PROCEDURE [dbo].[IHSI_CashAppLoop]
@DatabaseList VARCHAR(2000)

AS 

-------------- Testing --------
--DECLARE
--@DatabaseList varchar(2000) = 
----''
--'190' --PEPA
-----------------------------


SET NOCOUNT ON;
DECLARE @ctr int, @dbName VARCHAR (50), @SQL nvarchar(4000), @cpnyName char(50) 

IF @DatabaseList = ''
SET @DatabaseList = (
SELECT STUFF(list,1,1,'')
FROM (
SELECT ','+ CAST(CpnyID AS VARCHAR(16)) as [text()]
FROM dbo.IHSI_ActiveClients
WHERE ISNUMERIC (cpnyID) = 1
AND cpnyid NOT IN ('DV0','CL0', 'ED0', 'HP0')
FOR XML PATH('')
) AS Sub(list)
) 

 
IF OBJECT_ID('tempdb..#CashApp') IS NOT NULL
    DROP TABLE #CashApp

IF OBJECT_ID('tempdb..#tmpDBList') IS NOT NULL
    DROP TABLE #tmpDBList

CREATE TABLE #tmpDBList
(
RowID INT IDENTITY,
cpnyName CHAR (50),
DatabaseName VARCHAR (50) 
)

CREATE TABLE #CashApp 
(	Company CHAR(30)
	,ProviderAlias VARCHAR(44)
	,ProviderNumber CHAR(7)
	,PayorType VARCHAR(40)
	,PayorTypeCode CHAR(2)
	,EnteredDate DATE
	,PostedPeriod CHAR(6)
	,PaymentAmount FLOAT
)

INSERT INTO #tmpDBList
SELECT CpnyName, DatabaseName
FROM dbo.IHSI_ActiveClients
WHERE CpnyID IN (SELECT str FROM dbo.charlist_to_table(@DatabaseList,Default))




--initialize the counter to 1
SET @ctr = 1


--begin loop
WHILE @ctr <= (SELECT COUNT(*) FROM #tmpDBList) 
BEGIN
	--set database variable
	SET @dbName = (SELECT DatabaseName FROM #tmpDBList WHERE rowid = @ctr)
	SET @cpnyName = (SELECT CpnyName FROM #tmpDBList WHERE rowid = @ctr)
	SET @SQL = 'EXEC ' + @dbName + '.dbo.IHSI_CashApp'
	
	INSERT INTO #CashApp
	EXECUTE sp_executesql @SQL  
	
	
	
SET @ctr = @ctr + 1
--end loop
END

SELECT * FROM #CashApp

DROP TABLE #CashApp
DROP TABLE #tmpDBList



