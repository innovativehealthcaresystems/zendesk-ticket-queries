use TX_BSW_CIP

select
	DB_NAME() as DatabaseName
	,p.AliasName as ClientAccountNumber
	,p.MedicalRecordNo
	,p.Number as IHSIPatientNumber
	,cast(s.FromDate as date) as ServiceDate
from patient p
left join service s
	on p.Number = s.PatientNumber
    and s.Amount > 0
where p.AliasName <> ''
group by
	p.AliasName
	,p.MedicalRecordNo
	,p.Number
	,s.FromDate
order by p.Number asc