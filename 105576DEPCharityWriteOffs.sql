/*Create holding table for accounts pending adjustment*/
create table #tmpServices
	(ServiceNumber char(9) not null
	,PatientNumber char(9) not null
	,Charges float not null
	,ServiceStatusCode int not null
	,TotalPayments float
	,TotalOtherAdjustments float
	,TurnAmountOutside float not null
	,TurnAmountToGideon float
	,TurnedOutside char(1) not null
	,TurnedToGideon char(1)
	,CharityBalance decimal(10,2))
--------------------------------------------------------------



/*Fill the holding table with all pending services as well as 080 (turned outside) adjustments to save a step*/
insert into #tmpServices
	(ServiceNumber
	,PatientNumber
	,Charges
	,ServiceStatusCode
	,TurnAmountOutside
	,TurnedOutside)

select
	s.Number as ServiceNumber
	,s.PatientNumber
	,s.Amount * s.Units
	,s.ServiceStatusCode
	,isnull(sum(a.amount), 0) as TurnAmountOutside
	,case
		when count(a.amount) > 1 then 'Y'
		else 'N'
	end as TurnedOutside
from IHSI_DEPCharity dc
join Patient p
	on dc.AcctNo = p.AliasName
	and dc.Imported = 'N'
join Service s
	on p.Number = s.PatientNumber
	and s.amount * s.Units > 0.01
	and s.TransToSvcNum = ''
left join Adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '080'
where dc.OriginalHospitalService = 'EEM'
group by
	s.Number
	,s.PatientNumber
	,s.Amount
	,s.Units
	,s.ServiceStatusCode
order by s.PatientNumber asc
-----------------------------------------------------------------------------------



/*Gather the payments and update the holding table*/
declare @TotalPayments table
	(ServiceNumber char(9) not null
	,TotalPayments float not null)

insert into @TotalPayments
select
	ts.ServiceNumber
	,isnull(sum(pay.amount), 0)
from #tmpServices ts
left join Payment pay
	on ts.ServiceNumber = pay.ServiceNumber
group by ts.ServiceNumber

update ts
	set ts.TotalPayments = tp.TotalPayments
from #tmpServices ts
join @TotalPayments tp
	on ts.ServiceNumber = tp.ServiceNumber
--------------------------------------------------------------------



/*Gather the other adjustments (besides 080 and 045) and update the holding table*/
declare @TotalOtherAdjustments table
	(ServiceNumber char(9) not null
	,TotalOtherAdjustments float not null)

insert into @TotalOtherAdjustments
select
	ts.ServiceNumber
	,isnull(sum(a.amount), 0)
from #tmpServices ts
left join Adjustment a
	on ts.ServiceNumber = a.ServiceNumber
	and a.AdjustmentTypeCode not in ('080','045')
group by ts.ServiceNumber

update ts
	set ts.TotalOtherAdjustments = toa.TotalOtherAdjustments
from #tmpServices ts
join @TotalOtherAdjustments toa
	on ts.ServiceNumber = toa.ServiceNumber
---------------------------------------------------------------------



/*Gather the 045 (turned to Gideon) adjustments and update the holding table*/
declare @TurnAmountToGideon table
	(ServiceNumber char(9) not null
	,TurnAmountToGideon float not null
	,TurnedToGideon char(1) not null)

insert into @TurnAmountToGideon
select
	ts.ServiceNumber
	,isnull(sum(a.amount), 0)
	,case
		when count(a.Amount) > 0 then 'Y'
		else 'N'
	end as TurnedToGideon
from #tmpServices ts
left join Adjustment a
	on ts.ServiceNumber = a.ServiceNumber
	and a.AdjustmentTypeCode = '045'
group by ts.ServiceNumber

update ts
	set ts.TurnAmountToGideon = ttg.TurnAmountToGideon
	,ts.TurnedToGideon = ttg.TurnedToGideon
from #tmpServices ts
join @TurnAmountToGideon ttg
	on ts.ServiceNumber = ttg.ServiceNumber
-----------------------------------------------------------------------



/*Calculate the write off amounts (factors in the amount in the turn(s) even though they are about to be reversed)*/
update #tmpServices
set CharityBalance = Charges - TotalPayments + TotalOtherAdjustments
-----------------------------------------------------------------------



/*Reverse the turns*/
----------------------------------------------------------------------



/*Perform the write-offs*/
-----------------------------------------------------------------------



/*Update the service status codes*/
-----------------------------------------------------------------------



/*Note in the charity table that the adjustments have been performed*/
----------------------------------------------------------------------


/*Pass Non-ED's to someone else (not EEM on original hospital service) */
---------------------------------------------------------------------------


/*Provide list to GRM of affected accounts for forwarding/writing off*/
--------------------------------------------------------------------------

/*Testing*/
select
	*
from #tmpServices
---------------------------------------------------------------------



/*Drop the holding table*/
drop table #tmpServices


