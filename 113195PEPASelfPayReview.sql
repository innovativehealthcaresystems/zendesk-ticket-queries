/*
Variables/Parameters Needed:
*Client/Database
*Provider (standard filter)
*PayorNumber (individual option as well as 'All')
*VerifiedStartDate (date)
*VerifiedEndDate (date)
*/


-------------ED code below--------------------

/*ED Client Filter Here*/
begin

	select
		s.ProviderNumber
		,pro.OrganizationName
		,s.PatientNumber
		,st.Staging_ID
		,s.FromDate
		,s.PayorNumber
		,pay.Name as PayorName
		,s.ServiceStatusCode
		,st.VerifiedByID
		,sta.Name as VerifiedByName
		,st.VerifiedDate
	from service s with (nolock)
	left join IHSI_Staging st with (nolock) 
		on s.PatientNumber = st.PatientNumber
		--and st.verifieddate > VerifiedStartDate	--Use variables here
		--and st.verifieddate < dateadd(day,1,verifiedenddate)
	left join Provider pro with (nolock)
		on s.ProviderNumber = pro.Number
	left join Payor pay with (nolock)
		on s.PayorNumber = pay.Number
	left join staff sta with (nolock)
		on st.VerifiedByID = sta.Number
	--where...provider filter...payor filter	--Use provider and payor variables here

end
-------------Non-ED code below--------------------------
----else(non-ED clients)...begin...end
begin

select s.ProviderNumber
	   , p.OrganizationName
	   , s.PatientNumber
	   , cdr.Summary_ID
	   , s.FromDate as DOS
	   , s.PayorNumber
	   , pay.Name as PayorName
	   , s.ServiceStatusCode
	   , csr.VerifiedByID
	   , sta.Name as VerifiedByName
	   , csr.VerifiedDate
from Service s
--left join IHSI_ClinCodingEntries cce on cce.ServiceNumber = s.Number and cce.ServiceItemCode = s.ServiceItemCode
left join IHSI_ClinSummaryRecord csr on csr.PatientNumber = s.PatientNumber 
	--and csr.VerifiedDate > VerifiedStartDate	--Use variables here
	--and csr.VerifiedDate < DATEADD(day,1,VerifiedEndDate)
left join IHSI_ClinDetailRecord cdr on cdr.Summary_ID = csr.Summary_ID
left join Provider p on p.Number = s.ProviderNumber
left join Payor pay on pay.Number = s.PayorNumber
left join Staff sta on csr.VerifiedByID = sta.Number
--where...provider filter...payor filter	--Use provider and payor variables here
end