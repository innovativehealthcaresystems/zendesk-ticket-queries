select *
from (
		select sn.PatientNumber
			,count(distinct sn.serviceitemcode) as NewSICount
			,count(distinct so.ServiceItemCode) as OldSICount
		from service sn
		left join Adjustment a1
			on sn.Number = a1.ServiceNumber
				and a1.AdjustmentTypeCode = '019'
		left join service so
			on sn.PatientNumber = so.PatientNumber
				and so.Comment not like '%corrected%'
				and so.TransToSvcNum = ''
				and so.Amount * so.Units > 0.01
		where sn.PostedPeriod >= '201809'
			and sn.Comment like '%corrected%'
			and sn.TransToSvcNum = ''
			and a1.ServiceNumber is null
		group by sn.PatientNumber
)x
where x.OldSICount > 0
	and x.NewSICount < x.OldSICount