select *
into #tmpServices
from (
	select 
		s1.PatientNumber as InnovativePatientNumber
		,p1.AliasName as ClientAccountNumber
		,pay1.Name as OldPayor
		,pt1.Description as OldPayorType
		--Original claim date?
		,pay2.Name as NewPayor
		,pt2.Description as NewPayorType
		,c2.LastReprintDate as LastClaimDate
		,cast(s1.FromDate as date) as DOS
		,s2.Number as ServiceNumber
		,s2.ServiceItemCode as EandM
		,s2.Amount * s2.Units as Charge
		,isnull(sum(a2s.Amount), 0) as Adjustments
		--,isnull(sum(pmt.Amount), 0) + isnull(sum(a2p.Amount), 0) as Payments
		--,(s2.Amount * s2.Units) + isnull(sum(a2s.Amount), 0) - (isnull(sum(pmt.Amount), 0) + isnull(sum(a2p.Amount),	0)) as Balance
		,s2.ServiceStatusCode
	from service s1
	join payor pay1
		on s1.PayorNumber = pay1.Number
	join patient p1
		on s1.PatientNumber = p1.Number
	join IHSI_PayorType pt1
		on pay1.TypeCode = pt1.Code
	
	join service s2
		on s1.TransToSvcNum = s2.Number
	join Payor pay2
		on s2.PayorNumber = pay2.Number
	left join Adjustment a2s
		on s2.Number = a2s.ServiceNumber
		and a2s.TransType = 'S'
	--left join Payment pmt
	--	on s2.Number = pmt.ServiceNumber
	--left join Adjustment a2p
	--	on s2.Number = a2p.ServiceNumber
	--	and a2p.TransType = 'P'
	join IHSI_PayorType pt2
		on pay2.TypeCode = pt2.Code
	left join Claim c2
		on s2.ClaimNumber = c2.Number
	
	where s1.TransToSvcNum <> ''
		and pay1.TypeCode = 'A'
		and s1.FromDate >= '2018-01-01'
		and s1.ServiceItemCode >= '99281'
		and s1.ServiceItemCode <= '99285'
		and s2.TransToSvcNum = ''
		--and s2.PatientNumber = '000001170'
		and pt2.Code <> 'A'
	
	group by 	s1.PatientNumber
		,p1.AliasName
		,cast(s1.FromDate as date)
		,s2.ServiceItemCode
		,s2.Amount
		,s2.Units
		--,sum(a2s.Amount)
		--,sum(pmt.Amount) + sum(a2p.Amount)
		--,(s2.Amount * s2.Units) + sum(a2s.Amount) - (sum(pmt.Amount) + sum(a2p.Amount))
		,pay1.Name
		,pt1.Description
		--Original claim date?
		,pay2.Name
		,pt2.Description
		--Last claim date
		,s2.ServiceStatusCode
		,s2.Number
		,c2.LastReprintDate
) as x

select *
into #tmpStep2
from (
	select ts.InnovativePatientNumber
		,ts.ClientAccountNumber
		,ts.ServiceNumber
		,ts.OldPayor
		,ts.OldPayorType
		,ts.NewPayor
		,ts.NewPayorType
		,ts.DOS
		,ts.EandM
		,ts.Charge
		,ts.Adjustments
		,sum(isnull(pmt2.Amount, 0)) as Payments
		--,ts.Charge + ts.Adjustments - (sum(isnull(pmt2.Amount, 0)) + sum(isnull(a2p.amount,0))) as Balance
		,ts.ServiceStatusCode
		,ts.LastClaimDate
	from #tmpServices ts
	left join Payment pmt2
		on ts.ServiceNumber = pmt2.ServiceNumber
	--left join Adjustment a2p
	--	on ts.ServiceNumber = a2p.ServiceNumber
	--	and a2p.TransType = 'P'
	--where ts.InnovativePatientNumber = '000001170'
	group by ts.InnovativePatientNumber
		,ts.ClientAccountNumber
		,ts.OldPayor
		,ts.OldPayorType
		,ts.NewPayor
		,ts.NewPayorType
		,ts.DOS
		,ts.EandM
		,ts.Charge
		,ts.Adjustments
		,ts.ServiceStatusCode
		,ts.ServiceNumber
		,ts.LastClaimDate
) as y

select ts2.InnovativePatientNumber
	,ts2.ClientAccountNumber
	,ts2.OldPayor
	,ts2.OldPayorType
	,ts2.NewPayor
	,ts2.NewPayorType
	,ts2.DOS
	,cast(ts2.LastClaimDate as date) as LastClaimDate
	,ts2.EandM
	,ts2.Charge
	,ts2.Adjustments
	,ts2.Payments + sum(isnull(a2p.amount, 0)) as Payments
	,ts2.Charge + ts2.Adjustments - (ts2.Payments + sum(isnull(a2p.amount, 0))) as Balance
	,ts2.ServiceStatusCode
from #tmpStep2 ts2
left join Adjustment a2p
	on ts2.ServiceNumber = a2p.ServiceNumber
	and a2p.TransType = 'P'
group by ts2.InnovativePatientNumber
	,ts2.ClientAccountNumber
	,ts2.OldPayor
	,ts2.OldPayorType
	,ts2.NewPayor
	,ts2.NewPayorType
	,ts2.DOS
	,ts2.EandM
	,ts2.Charge
	,ts2.Adjustments
	,ts2.Payments
	,ts2.ServiceStatusCode
	,ts2.LastClaimDate

drop table #tmpStep2
drop table #tmpServices