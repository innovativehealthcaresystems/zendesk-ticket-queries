select s.PatientNumber
	,s.Number as ServiceNumber
	,s.FromDate
	,s.ServiceItemCode
from service s
join IHSI_PseudoCodes pc
	on s.ServiceItemCode = pc.Code
where s.FromDate >= '2018-12-22'
	and s.FromDate < '2018-12-23'
	and s.Amount = 0

