select 
	s1.patientnumber
	,s1.number
	,s2.admissionnumber
--update s1
--set s1.AdmissionNumber = s2.AdmissionNumber
--	,s1.LastUpdatedBy = 'T018'
--	,s1.LastUpdatedDate = getdate()
from service s1
	join service s2
		on s1.patientnumber = s2.patientnumber
			and s2.admissionnumber <> ''
			and cast(s1.fromdate as date) = cast(s2.fromdate as date) --For CL Clients
where s1.comment like '%corrected%' 
	and s1.admissionnumber = ''
group by s1.patientnumber
	,s1.number
	,s2.admissionnumber
