--select
--	s.Number
--	,s.PatientNumber
--	,s.LastUpdatedBy
--	,s.LastUpdatedDate
--	,s.ServiceStatusCode
--	,s.ProviderNumber
--	,s.Comment
--	,s.UserText3
--	,a.audit_changed
--	,a.ServiceStatusCode
--	,a2.audit_changed
--	,a2.ServiceStatusCode
update s
	set s.LastUpdatedBy = '9999'
		,s.LastUpdatedDate = GETDATE()
		,s.ServiceStatusCode = a.ServiceStatusCode
		,s.comment = case when s.Comment = 'HOLD PER CLIENT REQUEST' then ''
			else s.Comment end
		,s.UserText3 = ''
from service s
join AuditService a
	on s.Number = a.Number
	and a.ServiceStatusCode in (7,27)
left join AuditService a2
	on a.Number = a2.Number
	and a2.audit_changed > a.audit_changed
	and a2.ServiceStatusCode in (7,27)
where 
	s.UserText3 = 'uhc hold'
	and s.ProviderNumber <> '0000003'
	and a2.audit_changed is null
--order by 
--	s.number desc
--	,a.audit_changed desc