select
	s.PatientNumber
	,p.AliasName
	,p.MedicalRecordNo
	,csr.Summary_ID
	,csr.VisitID
	,cast(s.fromdate as date) as DOS
	--,s.ServiceItemCode
	--,s.Number as ServiceNumber
	--,s.Number
	----,cdr.Detail_ID
	----,cce.ServiceNumber
	----,cce.DateOfService
from service s
join patient p
	on s.PatientNumber = p.Number
	and p.AliasName <> ''
left join adjustment a
	on s.Number = a.ServiceNumber
	and a.AdjustmentTypeCode = '019'
left join IHSI_ClinSummaryRecord csr
	on p.Number = csr.PatientNumber
left join IHSI_ClinDetailRecord cdr
	on csr.Summary_ID = cdr.Summary_ID
left join IHSI_ClinCodingEntries cce
	on s.Number = cce.ServiceNumber
	and cce.Detail_ID = cdr.Detail_ID
	--and cast(s.fromdate as date) = cast(cce.DateOfService as date)
where 
	s.TransToSvcNum = ''
	and a.number is null
	--and MedicalRecordNo like '%01378'
	and cce.ServiceNumber is not null
group by
	s.PatientNumber
	,csr.Summary_ID
	,p.AliasName
	,p.MedicalRecordNo
	,csr.VisitID
	,cast(s.FromDate as date)
	--,s.ServiceItemCode
	--,s.Number
order by 
	p.MedicalRecordNo asc
	,csr.Summary_ID asc
	,csr.visitid asc
	,DOS asc
