	--select distinct p.AddressLine1	--Review Addresses Before Writing Off
	--	,p.AddressLine2
	--	,p.Number
	--	,s.Number
	--	,s.ServiceStatusCode
	--	,bbs.Balance
	--	,s.fromdate
	--from patient p
	--left join service s
	--	on p.Number = s.PatientNumber
	--	and s.TransToSvcNum = ''
	--	and s.ServiceStatusCode in (7,27)
	--left join payor pay
	--	on s.PayorNumber = pay.Number
	--left join IHSI_BalanceByPatient bbp
	--	on p.Number = bbp.PatientNumber
	--left join IHSI_BalanceByService bbs
	--	on s.Number = bbs.ServiceNumber
	--where (p.AddressLine1 like 'unk%'
	--		or p.AddressLine2 like 'ret%'
	--		or p.addressline1 like '%bad%add%'
	--		or p.addressline2 like 'bad%add%'
	--		or p.AddressLine1 like '%know%')
	--		and s.PatientNumber is not null
	--		and s.FromDate < dateadd(month, -6, getdate())
	--		and bbs.Balance > 0
-------------------------------Begin Process B------------------------


select *
into #tmpPatientUpdates
from
(
	select p.Number as PatientNumber
		,p.BillingStatusCode
		,s.Number as ServiceNumber
		,s.ServiceStatusCode
		,bbs.Balance as ServiceBalance
		,s.PayorNumber
	from patient p
	left join service s
		on p.Number = s.PatientNumber
		and s.TransToSvcNum = ''
		and s.ServiceStatusCode in (7,27)
	left join payor pay
		on s.PayorNumber = pay.Number
	left join IHSI_BalanceByPatient bbp
		on p.Number = bbp.PatientNumber
	left join IHSI_BalanceByService bbs
		on s.Number = bbs.ServiceNumber
	where (p.AddressLine1 like 'unk%'
			or p.AddressLine2 like 'ret%'
			or p.addressline1 like '%bad%add%'
			or p.addressline2 like 'bad%add%'
			or p.AddressLine1 like '%know%')
			and s.PatientNumber is not null
			and s.FromDate < dateadd(month, -6, getdate())
			and bbs.Balance > 0
	group by p.Number
		,p.BillingStatusCode
		,s.Number
		,s.ServiceStatusCode
		,bbs.Balance
		,s.PayorNumber
) as x

alter table #tmpPatientUpdates
add RowID smallint identity

alter table #tmpPatientUpdates
add AdjNumber char(9)

declare @NextAdjustmentNumber int
declare @TransDate smalldatetime
SET @NextAdjustmentNumber = (SELECT [NextNumber] FROM dbo.AdjustmentSequence)
UPDATE #tmpPatientUpdates
SET AdjNumber = RIGHT(RTRIM('000000000' + CAST((RowID) + @NextAdjustmentNumber AS VARCHAR)), 9)

SET @TransDate = CAST(FLOOR(CAST(getdate() AS FLOAT))AS SMALLDATETIME)

INSERT INTO dbo.Adjustment (AdjReasonCategory, AdjReasonCode, AdjustmentTypeCode, Amount, EnteredByID, 
	EnteredDate, Number, PaymentNumber, PayorNumber, Posted, PostedPeriod, PrintOnStmtFlag, ReasonText, 
	ServiceNumber, StatementNumber, SubAccount, TransType, UserDate1, UserDate2, UserFloat1, UserFloat2, UserText1, 
	UserText2, UserText3, UserText4)
SELECT '' AS AdjReasonCategory
	,'' AS AdjReasonCode 
	,'086' AS AdjustmentTypeCode
	,(ServiceBalance * -1) AS Amount
	,'T018' AS EnteredByID
	,@TransDate AS EnteredDate
	,AdjNumber AS Number
	,'' AS PaymentNumber
	,PayorNumber
	,'Y' AS Posted
	,(SELECT CurrentPeriodNumber FROM dbo.mbsetup) AS PostedPeriod
	,'Y' AS PrintOnStmtFlag
	,'PT ADDRESS UNKNOWN' AS ReasonText
	,ServiceNumber as ServiceNumber
	,'' AS StatementNumber
	,'' AS SubAccount
	,'S' as TransType
	,@TransDate AS UserDate1
	,'1900-01-01 00:00:00' as UserDate2
	,0 as UserFloat1
	,0 as UserFloat2
	,'' as UserText1
	,'' as UserText2
	,'' as UserText3
	,'' as UserText4
FROM #tmpPatientUpdates

UPDATE [dbo].[AdjustmentSequence]
SET [NextNumber] = (SELECT MAX(RIGHT(RTRIM('000000000' + Number), 9)) + 1 FROM Adjustment)

update s
	set s.ServiceStatusCode = 40
	,s.LastUpdatedBy = 'T018'
	,s.LastUpdatedDate = getdate()
from #tmpPatientUpdates tpu
join service s
	on tpu.ServiceNumber = s.Number

--select p.BillingStatusCode
--	,p.LastUpdatedBy
--	,p.LastUpdatedDate
--from #tmpPatientUpdates tpu
--join patient p
--	on tpu.PatientNumber = p.Number
go
DISABLE TRIGGER [dbo].[Patient_UTrig] ON [dbo].[Patient]
go

update p
	set p.BillingStatusCode = 'N'
	,p.LastUpdatedBy = 'T018'
	,p.LastUpdatedDate = getdate()
from #tmpPatientUpdates tpu
join Patient p
	on tpu.PatientNumber = p.Number
go 

enable TRIGGER [dbo].[Patient_UTrig] ON [dbo].[Patient]
go



SELECT name, is_disabled FROM sys.triggers	--Make sure trigger is reenabled
where name = 'Patient_UTrig'

--select * from #tmpPatientUpdates
drop table #tmpPatientUpdates



---------------------------------End Process B--------------------------


--------------------------------Begin Process A--------------------------

--DISABLE TRIGGER [dbo].[Patient_UTrig] ON [dbo].[Patient]
--go

--select *
--into #tmpPatientUpdates
--from
--(
--	select p.Number as PatientNumber
--		,p.CreditStatusCode
--		,p.BillingStatusCode
--		,bbp.Balance as PatientBalance
--		,case
--			when p.CreditStatusCode <> 8 then 'Y'
--			else 'N'
--		end as Updated
--	from patient p
--	left join service s
--		on p.Number = s.PatientNumber
--		and s.TransToSvcNum = ''
--	left join payor pay
--		on s.PayorNumber = pay.Number
--	left join IHSI_BalanceByPatient bbp
--		on p.Number = bbp.PatientNumber
--	left join service s2
--		on p.Number = s2.PatientNumber
--		and s2.TransToSvcNum = ''
--		and s.Number <> s2.Number
--	left join Payor pay2
--		on s2.PayorNumber = pay2.Number
--		and pay2.TypeCode in ('B','C','D')
--	where (p.AddressLine1 like 'unk%'
--			or p.AddressLine2 like 'ret%'
--			or p.addressline1 like '%bad%add%'
--			or p.addressline2 like 'bad%add%')
--		and pay.TypeCode not in ('B','C','D')
--		and pay2.Number is null
--		and (p.BillingStatusCode <> 'N' or p.CreditStatusCode <> 8)
--	group by p.Number
--		,p.CreditStatusCode
--		,p.BillingStatusCode
--		,bbp.Balance
--		,pay.TypeCode
--) as x

--update p
--set p.CreditStatusCode = 8
--	,p.BillingStatusCode = 'N'
--	,p.LastUpdatedBy = 'T018'
--	,p.LastUpdatedDate = GETDATE()
--from #tmpPatientUpdates tpu
--join patient p
--	on tpu.PatientNumber = p.number

--select * from #tmpPatientUpdates

--drop table #tmpPatientUpdates
--go

--enable TRIGGER [dbo].[Patient_UTrig] ON [dbo].[Patient]
--go

--SELECT name, is_disabled FROM sys.triggers	--Make sure trigger is reenabled
--where name = 'Patient_UTrig'

--select count(*) as Outliers	--Make sure nothing is left over
--from patient p
--left join service s
--	on p.Number = s.PatientNumber
--	and s.TransToSvcNum = ''
--left join payor pay
--	on s.PayorNumber = pay.Number
--left join IHSI_BalanceByPatient bbp
--	on p.Number = bbp.PatientNumber
--left join service s2
--	on p.Number = s2.PatientNumber
--	and s2.TransToSvcNum = ''
--	and s.Number <> s2.Number
--left join Payor pay2
--	on s2.PayorNumber = pay2.Number
--	and pay2.TypeCode in ('B','C','D')
--where (p.AddressLine1 like 'unk%'
--		or p.AddressLine2 like 'ret%'
--		or p.addressline1 like '%bad%add%'
--		or p.addressline2 like 'bad%add%')
--	and pay.TypeCode not in ('B','C','D')
--	and pay2.Number is null
--	and (p.BillingStatusCode <> 'N' or p.CreditStatusCode <> 8)
----------------------------------End Process A------------------------------