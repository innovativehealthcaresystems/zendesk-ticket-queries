select
	s.PatientNumber
	,s.Number
	,s.FromDate
	,s.EnteredDate
	,s.ServiceStatusCode
	,s.UserSvcStatusCode
	,s.LastUpdatedBy
	,s.LastUpdatedDate
	,ps.PrintedDate
	,max(pay.EnteredDate) as RecentPayorResponse
	,datediff(day, max(pay.EnteredDate), ps.PrintedDate) as DaysBetweenPayAndStatement
	,count(pay.number) as PaymentNumber
from AuditService aud1
join service s
	on aud1.Number = s.Number
join PatientStatement ps
	on s.StatementNumber = ps.Number
join payment pay
	on s.Number = pay.ServiceNumber
where aud1.LastUpdatedDate >= '2018-04-04'
	and aud1.LastUpdatedDate < '2018-04-05'
	and aud1.ServiceStatusCode = 7
	and aud1.LastUpdatedBy <> ''
	and aud1.FromDate < '2018-01-01'
	and s.ServiceStatusCode <> 40
group by 
	s.PatientNumber
	,s.Number
	,s.FromDate
	,s.EnteredDate
	,s.ServiceStatusCode
	,s.UserSvcStatusCode
	,s.LastUpdatedBy
	,s.LastUpdatedDate
	,ps.PrintedDate