
-------------Aggregates-----------------------
select
	sum(y.TotalBalanceAt27Over180Days) as TotalBalanceAt27Over180Days
	,max(y.ClientTurnsToGRM) as ClientTurnsToGRM
	,max(y.ClientTurnsOutside) as ClientTurnsOutside
	,max(y.LastStatementRun) as LastStatementRun
	,max(y.LastTurnDate) as LastTurnDate
from
(	
	select
	sum(x.Balance) as TotalBalanceAt27Over180Days
	,'' as ClientTurnsToGRM
	,'' as ClientTurnsOutside
	,'' as LastStatementRun
	,'' as LastTurnDate
from
(	
	select
	s.Number as ServiceNumber
	,s.PatientNumber
	,p.BillingStatusCode
	,bsc.Description
	,bs.Balance
	,s.FromDate as DOS
	,max(ps.PrintedDate) as LastStatementDate
	,max(aud.audit_changed) as StatusChangeDate
	,datediff(day,max(ps.printeddate),getdate()) as DaysSinceLastStatement
	,(select
		max(printeddate)
	from PatientStatement) as LastStatementRun
	,(select
		max(entereddate)
	from adjustment
	where AdjustmentTypeCode in ('045','080')) as LastTurnDate
from service s
join IHSI_BalanceByService bs
	on s.Number = bs.ServiceNumber
join PatientStatement ps
	on s.PatientNumber = ps.PatientNumber
join Patient p
	on s.PatientNumber = p.Number
join IHSI_BillingStatusCodes bsc
	on p.BillingStatusCode = bsc.Code
left join AuditService aud
	on s.Number = aud.Number
	and aud.ServiceStatusCode <> 27
where s.ServiceStatusCode in ('27')
group by
	s.Number
	,s.PatientNumber
	,p.BillingStatusCode
	,bsc.Description
	,bs.Balance
	,s.FromDate
)x
where x.DaysSinceLastStatement >= 180
	and x.StatusChangeDate <= x.LastStatementRun
	--and x.StatusChangeDate <= x.LastTurnDate
--order by x.StatusChangeDate desc

union

select
	'' TotalBalanceAt27Over180Days
	,case
		when count(a.number) > 0 then 'Y'
		else 'N'
	end as ClientTurnsToGRM
	,'' as ClientTurnsOutside
	,'' as LastStatementRun
	,max(a.EnteredDate) as LastTurnDate
from adjustment a
where a.AdjustmentTypeCode = '045'

union

select
	'' TotalBalanceAt27Over180Days
	,'' as ClientTurnsToGRM
	,case
		when count(a.number) > 0 then 'Y'
		else 'N'
	end as ClientTurnsOutside
	,'' as LastStatementRun
	,max(a.EnteredDate) as LastTurnDate
from adjustment a
where a.AdjustmentTypeCode = '080'

union

select
	''
	,''
	,''
	,max(PrintedDate)
	,''
from PatientStatement
)y