SELECT p.AliasName AS IHSIPatientNumber
	,s.UserText1 AS ClientAccountNumber
	,CAST(s.FromDate AS DATE) AS DOS
	,CAST(s.EnteredDate AS DATE) AS TurnDate
	,s.Amount * s.Units AS TurnAmount
	,SUM(ISNULL(sa.Amount,0)) as AdjustmentAmount
	,SUM(ISNULL(pay.Amount,0)) + SUM(ISNULL(pa.Amount,0)) AS TotalPaid
	,REPLACE (MAX(ISNULL(CAST(pay.EnteredDate AS DATE), '')), '1900-01-01', 'N/A')  AS LastPaidDate
	,s.Amount * s.Units + SUM(ISNULL(sa.Amount,0)) - (SUM(ISNULL(pay.Amount,0)) + SUM(ISNULL(pa.Amount,0))) as BalanceDue
FROM Service AS s
JOIN Patient AS p
	ON s.PatientNumber = p.Number
LEFT JOIN Payment AS pay
	ON s.Number = pay.ServiceNumber
LEFT JOIN Adjustment AS pa
	ON s.Number = pa.ServiceNumber
	AND pa.TransType = 'P'
LEFT JOIN Adjustment AS sa
	ON s.Number = sa.ServiceNumber
	AND sa.TransType = 'S'
WHERE s.ProviderNumber = '0000059'   ----This will be the client variable
GROUP BY p.AliasName
	,s.UserText1
	,CAST(s.FromDate AS DATE)
	,CAST(s.EnteredDate AS DATE)
	,s.Amount * s.Units
ORDER BY p.AliasName ASC
	,CAST(s.FromDate AS DATE) ASC